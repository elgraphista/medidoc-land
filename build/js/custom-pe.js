/* Custom JS */


var slider = new Slider("#planRange");

const sliderVal = document.getElementById("planRangeSliderVal");

    // Radios

    const radio1 = document.getElementById("optionsRadios1");
    const radio2 = document.getElementById("optionsRadios2");
    const radio3 = document.getElementById("optionsRadios3");
    const radio4 = document.getElementById("optionsRadios4");

    radio1.addEventListener('click', () => {
        messagePlan.textContent = 'S/ 50,00 por mes';
    })
    radio2.addEventListener('click', () => {
        messagePlan.textContent = 'S/ 47,5 por trimestre';
    })
    radio3.addEventListener('click', () => {
        messagePlan.textContent = 'S/ 255,00 por semestre';
    })
    radio4.addEventListener('click', () => {
        messagePlan.textContent = 'S/ 510,00 por año';
    })

slider.on("slide", function(sliderValue) {
    sliderVal.textContent = sliderValue;
    if (sliderValue <= 1) {
        planRangeSliderValLabel.textContent = 'profesional';
    } else if (sliderValue == 20) {
        planRangeSliderValLabel.textContent = 'o más profesionales'
    } else {
        planRangeSliderValLabel.textContent = 'profesionales'
    }

    const numberPro = sliderValue;
    const priceMensual = numberPro === 1 ? 'S/ 50,00 por mes':
                    numberPro >= 2 && numberPro <= 5 ? 'S/ 100,00 por mes':
                    numberPro >= 6 && numberPro <= 10 ? 'S/ 150,00 por mes':
                    numberPro >= 11 && numberPro <= 15 ? 'S/ 200,00 por mes':
                    numberPro >= 16 && numberPro <= 20 ? 'S/ 250,00 por mes': 'S/ 20,000 por mes';

    const priceTrimestral = numberPro === 1 ? 'S/ 47,5 por trimestre':
                    numberPro >= 2 && numberPro <= 5 ? 'S/ 285,00 por trimestre':
                    numberPro >= 6 && numberPro <= 10 ? 'S/ 427,5 por trimestre':
                    numberPro >= 11 && numberPro <= 15 ? 'S/ 570,00 por trimestre':
                    numberPro >= 16 && numberPro <= 20 ? 'S/ 712,5 por trimestre': 'S/ 47,5 por trimestre';

    const priceSemestral = numberPro === 1 ? 'S/ 255,00 por semestre':
                    numberPro >= 2 && numberPro <= 5 ? 'S/ 510,00 por semestre':
                    numberPro >= 6 && numberPro <= 10 ? 'S/ 765,00 por semestre':
                    numberPro >= 11 && numberPro <= 15 ? 'S/ 1020,00 por semestre':
                    numberPro >= 16 && numberPro <= 20 ? 'S/ 1275,00 por semestre': 'S/ 255,00 por semestre';

    const priceAnual = numberPro === 1 ? 'S/ 510,00 por año':
                    numberPro >= 2 && numberPro <= 5 ? 'S/ 1020,00 por año':
                    numberPro >= 6 && numberPro <= 10 ? 'S/ 1530,00 por año':
                    numberPro >= 11 && numberPro <= 15 ? 'S/ 2040,00 por año':
                    numberPro >= 16 && numberPro <= 20 ? 'S/ 2550,00 por año': 'S/ 510,00 por año';

    messagePlan.textContent = priceMensual;

    // Radios

    radio1.addEventListener('click', () => {
        messagePlan.textContent = priceMensual;
    })
    radio2.addEventListener('click', () => {
        messagePlan.textContent = priceTrimestral;
    })
    radio3.addEventListener('click', () => {
        messagePlan.textContent = priceSemestral;
    })
    radio4.addEventListener('click', () => {
        messagePlan.textContent = priceAnual;
    })


    if (radio1.checked == true) {
        messagePlan.textContent = priceMensual;
    }
    else if (radio2.checked == true) {
        messagePlan.textContent = priceTrimestral;
    }
    else if (radio3.checked == true) {
        messagePlan.textContent = priceSemestral;
    }
    else if (radio4.checked == true) {
        messagePlan.textContent = priceAnual;
    }
    else {
        // DO NOTHING
        } 
});