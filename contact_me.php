<?php
if($_POST)
{
	$to_Email   	= "avasquez@medidoc.cl"; //Replace with recipient email address
	$subject        = 'Mensaje desde el landing '.$_SERVER['SERVER_NAME']; //Subject line for emails
	
	
	//check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	
		//exit script outputting json data
		$output = json_encode(
		array(
			'type'=>'error', 
			'text' => 'Request must come from Ajax'
		));
		
		die($output);
    } 
	
	//check $_POST vars are set, exit if any missing
	if(!isset($_POST["userName"]) || !isset($_POST["userEmail"]) || !isset($_POST["userMessage"]))
	{
		$output = json_encode(array('type'=>'error', 'text' => 'Campos de texto vienen vacíos!'));
		die($output);
	}

	//Sanitize input data using PHP filter_var().
	$user_Name        = filter_var($_POST["userName"], FILTER_SANITIZE_STRING);
	$user_Email       = filter_var($_POST["userEmail"], FILTER_SANITIZE_EMAIL);
	$user_Phone        = filter_var($_POST["userPhone"], FILTER_SANITIZE_STRING);
	$user_Center     = filter_var($_POST["userMessage"], FILTER_SANITIZE_STRING);
	
	$user_Center = str_replace("\&#39;", "'", $user_Center);
	$user_Center = str_replace("&#39;", "'", $user_Center);
	
	//additional php validation
	if(strlen($user_Name)<4) // If length is less than 4 it will throw an HTTP error.
	{
		$output = json_encode(array('type'=>'error', 'text' => '¡El nombre es demasiado corto o está vacío!'));
		die($output);
	}
	if(!filter_var($user_Email, FILTER_VALIDATE_EMAIL)) //email validation
	{
		$output = json_encode(array('type'=>'error', 'text' => '¡Por favor introduzca una dirección de correo electrónico válida!'));
		die($output);
	}
	if(strlen($user_Center)<5) //check emtpy message
	{
		$output = json_encode(array('type'=>'error', 'text' => 'El nombre del centro asistencial es demasiado corto'));
		die($output);
	}
	
	//proceed with PHP email.
	$headers = 'From: '.$user_Email.'' . "\r\n" .
	'Reply-To: '.$user_Email.'' . "\r\n" .
	'X-Mailer: PHP/' . phpversion();
	
	$sentMail = @mail($to_Email, $subject.'-- '.$user_Name. "\r\n" .'-- '.$user_Email, $headers);
	
	if(!$sentMail)
	{
		$output = json_encode(array('type'=>'error', 'text' => 'No se pudo enviar el correo! Por favor revise su configuración de correo PHP.'));
		die($output);
	}else{
		$output = json_encode(array('type'=>'message', 'text' => '¡Hola '.$user_Name .'! ¡Pronto nos pondremos en contacto contigo!'));
		die($output);
	}
}
?>